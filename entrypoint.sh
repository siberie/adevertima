#!/usr/bin/env bash

pushd /app/src
python init.py &
popd

gunicorn wsgi --reload --log-config /app/config/logging.conf --config /app/config/gunicorn.conf