import unittest as ut
import requests

endpoints = {
    'count': '/viewer-counter/',
    'avg_age': '/avg-age/',
    'gender-dist': '/gender-dist/'
}

test_vectors = [
    {
        'params': {
            'content': 1,
            'device': 1,
            'start': '2016-01-01 00:00:00',
            'end': '2016-01-01 00:50:00'
        },
        'res': {
            'common': {
                'content_id': 1,
                'device_id': 1,
                'start': '2016-01-01 00:00:00',
                'end': '2016-01-01 00:50:00'
            },
            'views': 9,
            'avg_age': 42,
            'gender-dist': {
                'female': 0.67,
                'male': 0.33
            }
        }
    },
    {
        'params': {
            'content': 1,
            'device': 1,
            'start': '2017-01-01 00:00:00',
            'end': '2017-01-01 00:50:00'
        },
        'res': {
            'common': {
                'content_id': 1,
                'device_id': 1,
                'start': '2017-01-01 00:00:00',
                'end': '2017-01-01 00:50:00'
            },
            'views': 0,
            'avg_age': 0,
            'gender-dist': {
                'female': 0,
                'male': 0
            }
        }
    }
]


class SimpleTests(ut.TestCase):
    def test_viewer_counter(self):
        for vector in test_vectors:
            data = requests.get('http://api:8000' + endpoints['count'], params=vector['params']).json()
            resp = dict(vector['res']['common'])
            resp.update({'views': vector['res']['views']})
            self.assertDictEqual(data, resp)

    def test_age_avg(self):
        for vector in test_vectors:
            data = requests.get('http://api:8000' + endpoints['avg_age'], params=vector['params']).json()
            resp = dict(vector['res']['common'])
            resp.update({'avg_age': vector['res']['avg_age']})
            self.assertDictEqual(data, resp)

    def test_gender_dist(self):
        for vector in test_vectors:
            data = requests.get('http://api:8000' + endpoints['gender-dist'], params=vector['params']).json()
            resp = dict(vector['res']['common'])
            resp.update({'gender-dist': vector['res']['gender-dist']})
            self.assertDictEqual(data, resp)


if __name__ == '__main__':
    ut.main()
