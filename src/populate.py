import pandas as pd
from datetime import datetime
from collections import namedtuple
from typing import Dict, List, Generator

Person = namedtuple('Person', ['start', 'end', 'age', 'gender'])
Event = namedtuple('Event', ['device', 'content', 'viewers', 'start', 'stop'])


def create_persons_coroutine(persons: pd.DataFrame) -> Generator[Dict[int, List[Person]], pd.Timestamp, None]:
    """
    yields a dictionary of persons who appeared in front of devices since the last call - grouped by device

    :param persons: persons pandas.DataFrame columns=(device_id, start, end, age, gender), index=start
    :return: dictionary {$device_id : [Person, Person,... ]}
    """
    time = pd.Timestamp(0)
    ret = {}
    for start, *person in persons.itertuples():
        device_id = person[0]
        if start < time:
            ret.setdefault(device_id, list())
            ret[device_id].append(Person(start, *person[1:]))

        else:
            while time <= start:
                time = (yield ret)
                ret = {}

            ret = {device_id: [Person(start, *person[1:])]}

    return ret


def populated_data(n: int = None) -> Generator[Event, None, None]:
    """
    Heuristic join of events with persons.
    Yields an Event tuple

    :param n: n first rows of evens.csv only, default - all rows
    :yield: Event(device, content, viewers, start, end)
    """

    print('parsing csv files...')
    events = pd.read_csv('../data/events.csv', header=None, names=['content', 'device', 'op', 'time'],
                         index_col=3, parse_dates=['time'], nrows=n,
                         ).sort_index()
    persons = pd.read_csv('../data/persons.csv', header=None, names=['device', 'start', 'end', 'age', 'gender'],
                          index_col=1, parse_dates=['start', 'end'],
                          ).sort_index()
    print('parsing csv files... done')
    persons_coroutine = create_persons_coroutine(persons)
    next(persons_coroutine)

    def fetch_persons_before(time: pd.Timestamp) -> Dict[int, List[Person]]:
        """
        helper function that operates persons coroutine

        :param time: current timestamp
        :return:
        """
        try:
            recent_persons = persons_coroutine.send(time)
        except StopIteration as e:
            recent_persons = {}

        return recent_persons

    event_by_device = {}
    persons_by_device = {}

    def update_persons_by_device(time: pd.Timestamp):
        """
        side effect - modifies event_by_device

        :param time: current Timestamp
        :out event_by_device:
        """
        recent = fetch_persons_before(time)

        for device, viewers in recent.items():
            persons_by_device.setdefault(device, [])
            persons_by_device[device].extend(viewers)

    print('joining data...')
    for time, content, device, op in events.itertuples():
        update_persons_by_device(time)

        if op == 'start':
            persons_by_device.setdefault(device, [])
            persons_by_device[device] = list(filter(lambda x: x.end > time, persons_by_device[device]))

            event_by_device[device] = Event(
                device,
                content,
                None,
                time,
                None
            )
        else:
            device, content, _, start, _ = event_by_device[device]
            viewers = persons_by_device[device]
            if len(viewers):
                yield Event(device, content, viewers, start, time)
            del event_by_device[device]

    print('joining data... done')

if __name__ == '__main__':
    x = list(populated_data())
