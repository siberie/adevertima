import views

from core.utils import url

routes = [
    url('/viewer-counter/', views.ViewerCountView),
    url('/avg-age/', views.AverageAgeView),
    url('/gender-dist/', views.GenderDistributionView)
]


