from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, DateTime, String
from sqlalchemy.dialects.mysql import TINYINT,INTEGER
from core.models import Base
from jsonlogging import JSONFormatter



class Event(Base):
    __tablename__ = 'events'

    content_id = Column(Integer, primary_key=True)
    device_id = Column(Integer, primary_key=True)
    start = Column(DateTime, primary_key=True)
    end = Column(DateTime, primary_key=True)



class Person(Base):
    __tablename__ = 'persons'

    device_id = Column(Integer, primary_key=True)
    gender = Column(Integer, primary_key=True)
    age = Column(Integer, primary_key=True)
    start = Column(DateTime, primary_key=True)
    end = Column(DateTime, primary_key=True)


class Status(Base):
    __tablename__ = 'status'

    key = Column(String(32), primary_key=True)
    value = Column(String(32))

class EventPerson(Base):
    __tablename__ = 'event_person'
    device_id = Column(TINYINT(unsigned=True))
    content_id = Column(TINYINT(unsigned=True))
    start = Column(INTEGER(unsigned=True))
    end = Column(INTEGER(unsigned=True))
    gender = Column(TINYINT(1, unsigned=True))
    age = Column(TINYINT(unsigned=True))
    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
