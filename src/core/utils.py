import rethinkdb as r
from rethinkdb import ReqlNonExistenceError, ReqlOpFailedError



def url(path, view, name=None):
    return {'path': path, 'view': view}


def is_initialized(conn):
    try:
        initialized = r.table('status').get('initialized')['value'].run(conn)
    except (ReqlOpFailedError, ReqlNonExistenceError):
        initialized = False
    return initialized
