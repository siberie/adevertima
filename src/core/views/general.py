from . import View
from falcon import HTTP_200
from json import dumps as serialize, JSONEncoder
from datetime import datetime


class DefaultEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return str(obj)
        return JSONEncoder.default(self, obj)


class JSONView(View):
    schema = None
    encoder = DefaultEncoder

    def respond(self, response, data, status=HTTP_200, content_type='application/json; charset=utf-8'):
        response.content_type = content_type
        response.data = bytes(serialize(data, cls=self.encoder), encoding='utf-8')