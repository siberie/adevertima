from ..decorators import cached_property
from inspect import ismethod
from functools import wraps


class View:
    def on_options(self, request, response, *args, **kwargs):
        response['Allow'] = ", ".join(self._allowed_methods)
        response['Content-Length'] = '0'

    @cached_property
    def _allowed_methods(self):
        return [name[3:] for name in dir(self) if name.startswith('on_') and ismethod(getattr(self, name))]
