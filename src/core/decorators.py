from functools import update_wrapper

class cached_property(object):
    """
    Decorator that converts a method with a single self argument into a
    property cached on the instance.

    Optional ``name`` argument allows you to make cached properties of other
    methods. (e.g.  url = cached_property(get_absolute_url, name='url') )
    """
    def __init__(self, func, name=None):
        self.func = func
        update_wrapper(self, func)

    def __get__(self, instance, type=None):
        if instance is None:
            return self
        res = instance.__dict__[self.name] = self.func(instance)
        return res




def schema(params):
    def inner(cls):
        if not hasattr(cls, 'params_schema'):
            setattr(cls, 'params_schema', params)
        return cls
    return inner
