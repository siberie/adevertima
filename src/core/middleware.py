from marshmallow import ValidationError
from falcon.errors import HTTPBadRequest
import json


class MarshmallowGetParameters(object):
    def process_resource(self, req, resp, resource, params):
        try:
            result, errors = resource.params_schema(strict=True).load(req.params)
        except ValidationError as e:
            raise HTTPBadRequest(description=json.dumps(e.messages))
        except AttributeError:
            pass
        else:
            req.params.update(result)


