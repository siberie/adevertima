from core.decorators import schema
from marshmallow import Schema, fields
from core.utils import is_initialized
from core.views.general import JSONView
import rethinkdb as r
from falcon.errors import HTTPServiceUnavailable


class Parameters(Schema):
    start = fields.DateTime(required=True)
    end = fields.DateTime(required=True)
    device = fields.Integer(required=True)
    content = fields.Integer(required=True)


def check_db_initialization(conn):
    if not is_initialized(conn):
        raise HTTPServiceUnavailable(title='Regenerating caches',
                                     description='The service is unavailable due to ongoing cache regeneration',
                                     retry_after=300)


@schema(params=Parameters)
class ViewerCountView(JSONView):
    def on_get(self, request, response):
        params = request.params
        device = params['device']
        content = params['content']
        start = params['start'].timestamp()
        end = params['end'].timestamp()

        try:
            with r.connect('db', db='advertima') as conn:
                check_db_initialization(conn)

                views = r.table('events').get([device, content])['viewers'].filter(
                    (r.row['start'] >= start).and_(r.row['end'] <= end)
                ).count().run(conn)
        except r.ReqlNonExistenceError:
            views = 0

        res = {
            'start': params['start'],
            'end': params['end'],
            'device_id': device,
            'content_id': content,
            'views': views,
        }

        self.respond(response, res)


@schema(params=Parameters)
class AverageAgeView(JSONView):
    def on_get(self, request, response):
        params = request.params
        device = params['device']
        content = params['content']
        start = params['start'].timestamp()
        end = params['end'].timestamp()

        try:
            with r.connect('db', db='advertima') as conn:
                check_db_initialization(conn)

                avg_age = r.table('events').get([device, content])['viewers'].filter(
                    (r.row['start'] >= start).and_(r.row['end'] <= end)
                )['age'].avg().run(conn)
        except r.ReqlNonExistenceError:
            avg_age = 0

        res = {
            'start': params['start'],
            'end': params['end'],
            'device_id': device,
            'content_id': content,
            'avg_age': avg_age,
        }

        self.respond(response, res)


@schema(params=Parameters)
class GenderDistributionView(JSONView):
    def on_get(self, request, response):
        params = request.params
        device = params['device']
        content = params['content']
        start = params['start'].timestamp()
        end = params['end'].timestamp()

        try:
            with r.connect('db', db='advertima') as conn:
                check_db_initialization(conn)

                female_ratio = r.table('events').get([device, content])['viewers'].filter(
                    (r.row['start'] >= start).and_(r.row['end'] <= end)
                )['gender'].avg().run(conn)
                male_ratio = 1.0 - female_ratio
        except r.ReqlNonExistenceError:
            female_ratio = 0
            male_ratio = 0

        res = {
            'start': params['start'],
            'end': params['end'],
            'device_id': device,
            'content_id': content,
            "gender-dist": {
                "male": round(male_ratio, 2),
                "female": round(female_ratio, 2)
            }
        }

        self.respond(response, res)
