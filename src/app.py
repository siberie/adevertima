import falcon as f
from urls import routes

from core.middleware import MarshmallowGetParameters


def get_application():
    api = f.API(middleware=[MarshmallowGetParameters()])
    for route in routes:
        api.add_route(route['path'], route['view']())

    return api

