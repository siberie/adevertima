import rethinkdb as r
from core.utils import is_initialized
from populate import populated_data

with r.connect('db', db='advertima') as conn:
    initialized = is_initialized(conn)

    def initialize():
        try:
            r.db_drop('advertima').run(conn)
        except r.RqlRuntimeError:
            pass
        r.db_create('advertima').run(conn)
        r.table_create('events', primary_key='key').run(conn)

        data = {}
        for device_id, content_id, viewers, start, end in populated_data():
            key = (int(device_id), int(content_id))
            data.setdefault(key, [])
            data[key].extend(map(
                lambda x: {
                    'start': max(start, x.start).timestamp(),
                    'end': min(end, x.end).timestamp(),
                    'age': int(x.age),
                    'gender': 1 if x.gender == 'female' else 0
                }, viewers)
            )

        counter = 0
        last = 0
        print('populating database...')
        for k, v in data.items():
            r.table('events').insert({'key': k, 'viewers': v}).run(conn)
            counter += 1
            new = int(round(counter/len(data),2)*100)
            if last != new:
                print('{}% done'.format(new))
                last = new

        print('populating database... done')

        r.table_create('status', primary_key='key').run(conn)
        r.table('status').insert({'key': 'initialized', 'value': True}).run(conn)


    if not initialized:
        print('starting database init...')
        initialize()
        print('database init done')
    else:
        print('database initialized')
