FROM python:3

WORKDIR /app/src

ADD requirements.txt /requirements.txt
RUN pip install -r /requirements.txt



EXPOSE 8000

ENTRYPOINT /app/entrypoint.sh




